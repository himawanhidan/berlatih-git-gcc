<?php

// require ('animal.php');
// require ('ape.php');
require('frog.php');

echo "Hewan 1 : <br>";
$sheep = new Animal("Shaun");
echo " Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 2
echo "Cold Blooded: $sheep->cold_blooded <br><br>"; // false  

echo "Hewan 2 : <br>";
$kodok = new Frog("buduk");
echo " Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>"; // 4
echo "Cold Blooded: $kodok->cold_blooded <br>";
echo "Jump : $kodok->jump<br><br>"; // "hop hop"

echo "Hewan 3 : <br>";
$sungokong = new Ape("Kera Sakti");
echo " Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>"; // 2
echo "Cold Blooded: $sungokong->cold_blooded <br>";
echo "Yell : $sungokong->yell<br><br>"; // "Auooo"
